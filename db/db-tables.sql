DROP DATABASE IF EXISTS addonis;
CREATE DATABASE IF NOT EXISTS addonis;
USE addonis;
create or replace table details
(
	details_id int auto_increment
		primary key,
	open_issues_count int null,
	pull_requests_count int null,
	last_commit_date date null,
	last_commit_message varchar(300) null
);

create or replace table roles
(
	role_id int auto_increment
		primary key,
	role varchar(20) null
);

create or replace table states
(
	state_id int auto_increment
		primary key,
	state varchar(15) default 'pending' not null
);

create or replace table tags
(
	tag_id int auto_increment
		primary key,
	tag_name varchar(50) not null
);

create or replace table users
(
	user_id int auto_increment
		primary key,
	first_name varchar(30) null,
	last_name varchar(30) null,
	username varchar(20) not null,
	password varchar(20) not null,
	phone_number int(10) not null,
	email varchar(50) not null,
	photo longblob null,
	enabled tinyint(1) default 1 null,
	role_id int null,
	constraint users_email_uindex
		unique (email),
	constraint users_phone_number_uindex
		unique (phone_number),
	constraint users_username_uindex
		unique (username),
	constraint users_roles_role_id_fk
		foreign key (role_id) references roles (role_id)
);

create or replace table ratings
(
	rating_id int auto_increment
		primary key,
	user_id int null,
	rating int null,
	addon_id int null,
	constraint ratings_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

create or replace table addons
(
	addon_id int auto_increment
		primary key,
	addon_name varchar(50) not null,
	description varchar(300) not null,
	downloads_count int null,
	origin_link varchar(200) not null,
	binary_content longblob null,
	image blob null,
	date_created date null,
	creator_id int not null,
	details_id int null,
	state_id int null,
	download_link varchar(100) null,
	ide varchar(30) null,
	category varchar(20) not null,
	rating_id int null,
	tag_id int null,
	constraint addons_name_uindex
		unique (addon_name),
	constraint addons_details_details_id_fk
		foreign key (details_id) references details (details_id),
	constraint addons_ratings_rating_id_fk
		foreign key (rating_id) references ratings (rating_id),
	constraint addons_states_state_id_fk
		foreign key (state_id) references states (state_id),
	constraint addons_tags_tag_id_fk
		foreign key (tag_id) references tags (tag_id),
	constraint addons_users_id_fk
		foreign key (creator_id) references users (user_id)
);

