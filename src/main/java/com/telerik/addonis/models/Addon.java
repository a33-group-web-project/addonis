package com.telerik.addonis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Set;

@Entity
@Table(name = "addons")
public class Addon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addon_id")
    private int id;

    @Column(name = "addon_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "downloads_count")
    private int numberOfDownloads;

    @Column(name = "origin_link")
    private String originLink;


    @JsonIgnore
    @Lob
    @Column(name = "binary_content", columnDefinition = "BLOB")
    private byte[] binaryContent;


    @JsonIgnore
    @Lob
    @Column(name = "image", columnDefinition = "BLOB")
    private byte[] image;


    @Column(name = "date_created")
    private LocalDate dateOfCreation;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    //    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "details_id")
    private Details details;


    //    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "state_id")
    private State state;

    @Column(name = "download_link")
    private String downloadLink;

    @Column(name = "ide")
    private String ide;

    @Column(name = "category")
    private String category;

    @ManyToOne
    @JoinColumn(name = "rating_id")
    private Rating rating;


    @OneToOne
    @JoinColumn(name = "tag_id")
    private Tags tags;

    public Addon() {

    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }


    public byte[] getBinaryContent() {
        return binaryContent;
    }

    public void setBinaryContent(byte[] binaryContent) {
        this.binaryContent = binaryContent;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public Details getDetails() {
        return details;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfDownloads() {
        return numberOfDownloads;
    }

    public void setNumberOfDownloads(int numberOfDownloads) {
        this.numberOfDownloads = numberOfDownloads;
    }

    public String getOriginLink() {
        return originLink;
    }

    public void setOriginLink(String originLink) {
        this.originLink = originLink;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateCreated) {
        this.dateOfCreation = dateCreated;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String download) {
        this.downloadLink = download;
    }

    public String getIde() {
        return ide;
    }

    public void setIde(String ide) {
        this.ide = ide;
    }

    @JsonIgnore
    public LocalDate getLastCommit() {
        return details.getLastCommit();
    }

    @JsonIgnore
    public String getPhotoBytes() {
        String imageBytes = "data:image/png;base64," + Base64.getEncoder().encodeToString(getImage());
        return imageBytes;
    }

    @JsonIgnore
    public String getBinaryBytes() {
        String binaryBytes = "data:image/png;base64," + Base64.getEncoder().encodeToString(getBinaryContent());
        return binaryBytes;
    }

}
