package com.telerik.addonis.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;

@Entity
@Table(name = "details")
public class Details {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "details_id")
    private int id;

    @Column(name = "open_issues_count")
    private int openIssues;

    @Column(name = "pull_requests_count")
    private int pullRequest;

    @Column(name = "last_commit_date")
    private LocalDate lastCommit;

    @Column(name = "last_commit_message")
    private String lastCommitMessage;


    public Details() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOpenIssues() {
        return openIssues;
    }

    public void setOpenIssues(int openIssues) {
        this.openIssues = openIssues;
    }

    public int getPullRequest() {
        return pullRequest;
    }

    public void setPullRequest(int pullRequest) {
        this.pullRequest = pullRequest;
    }

    public LocalDate getLastCommit() {
        return lastCommit;
    }

    public void setLastCommit(LocalDate lastCommit) {
        this.lastCommit = lastCommit;
    }

    public String getLastCommitMessage() {
        return lastCommitMessage;
    }

    public void setLastCommitMessage(String lastCommitMessage) {
        this.lastCommitMessage = lastCommitMessage;
    }


}
