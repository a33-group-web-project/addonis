package com.telerik.addonis.models.dto;

public class SearchUserDto {

    public String keyword;


    public SearchUserDto() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
