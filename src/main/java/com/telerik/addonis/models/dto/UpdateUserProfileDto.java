package com.telerik.addonis.models.dto;

import java.util.Base64;

public class UpdateUserProfileDto extends UpdateUserDetailsDto{

    private byte[] photo;

    public UpdateUserProfileDto() {
        super();
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoBytes(){
        String imageBytes = "data:image/png;base64," + Base64.getEncoder().encodeToString(getPhoto());
        return imageBytes;
    }
}
