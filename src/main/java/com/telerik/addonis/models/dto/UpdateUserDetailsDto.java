package com.telerik.addonis.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDetailsDto {

    @NotNull(message = "First name can't be empty")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols")
    private String firstName;

    @NotNull(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols")
    private String lastName;

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 50, message = "Password should be between 8 and 50 symbols")
    private String password;

    @NotNull(message = "Phone number can't be empty")
    @Size(max = 10, message = "Phone number should be maximum 10 digits")
    private String phoneNumber;

    @NotNull(message = "Email can't be empty")
    @Email
    @Size(min = 5, max = 50, message = "Email should be between 5 and 50 symbols")
    private String email;

    public UpdateUserDetailsDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
