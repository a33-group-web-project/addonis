package com.telerik.addonis.models.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.State;

import com.telerik.addonis.models.Tags;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Lob;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Base64;

public class AddonDto {


    @NotNull(message = "Addon name can't be empty")
    @Size(min = 3, max = 30, message = "Addon name should be between 3 and 30 symbols")
    private String name;

    @NotNull(message = "Description can't be empty")
    @Size(min = 5, max = 300, message = "Description should be between 5 and 300 symbols")
    private String description;

    @PositiveOrZero(message = "Number of downloads should be positive or zero.")
    private int numberOfDownloads;

    @NotNull(message = "Origin link can't be empty")
    private String originLink;

    @Lob
    @Transient
    @JsonIgnore
    private byte[] binaryContent;

    @Lob
    @Transient
    @JsonIgnore
    private byte[] image;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateOfCreation;

    //    @JsonIgnore
    @Transient
    private int creator;

    @Transient
    private int details;

    @Transient
    private State state;

    @PositiveOrZero(message = "Tag ID should be positive or zero")
    private int tags;


    @Transient
    private String downloadLink;

    @NotNull(message = "Addon ide can't be empty")
    private String ide;

    @Transient
    private String category;

    @Transient
    private Rating rating;

    public AddonDto() {

    }

    public int getTags() {
        return tags;
    }

    public void setTags(int tags) {
        this.tags = tags;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public int getDetails() {
        return details;
    }

    public void setDetails(int details) {
        this.details = details;
    }

    public int getNumberOfDownloads() {
        return numberOfDownloads;
    }

    public void setNumberOfDownloads(int numberOfDownloads) {
        this.numberOfDownloads = numberOfDownloads;
    }

    public String getOriginLink() {
        return originLink;
    }

    public void setOriginLink(String originLink) {
        this.originLink = originLink;
    }

    public byte[] getBinaryContent() {
        return binaryContent;
    }

    public void setBinaryContent(byte[] binaryContent) {
        this.binaryContent = binaryContent;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getIde() {
        return ide;
    }

    public void setIde(String ide) {
        this.ide = ide;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    @JsonIgnore
    public String getPhotoBytes() {
        String imageBytes = "data:image/png;base64," + Base64.getEncoder().encodeToString(getImage());
        return imageBytes;
    }

    //    @JsonIgnore
    public String getBinaryBytes() {
        String binaryBytes = "data:binary/png;base64," + Base64.getEncoder().encodeToString(getBinaryContent());
        return binaryBytes;
    }
}
