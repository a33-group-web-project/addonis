package com.telerik.addonis.models.dto;

public class TagsDto {



    private String[] tags;

    public TagsDto() {
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }


}

