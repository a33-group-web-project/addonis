package com.telerik.addonis.models.dto;

public class SearchAddonDto {


    public String keyword;


    public SearchAddonDto() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
