package com.telerik.addonis.models;

import javax.persistence.*;

@Entity
@Table(name = "ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "addon_id")
    private Addon addon;

    @JoinColumn(name = "rating")
    private int rating;

    public Rating() {
    }

    public Rating(int id, User user, Addon addon, int rating) {
        this.id = id;
        this.user = user;
        this.addon = addon;
        this.rating = rating;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Addon getAddon() {
        return addon;
    }

    public void setAddon(Addon addon) {
        this.addon = addon;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        int sum = rating + this.rating;
        this.rating = sum / 2;
    }

}