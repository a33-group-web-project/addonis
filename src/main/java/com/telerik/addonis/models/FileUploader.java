package com.telerik.addonis.models;

import javax.persistence.*;

@Entity
public class FileUploader {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private String fileName;
    private String filetype;

    @Lob
    private byte[] data;

    public FileUploader(String fileName, String filetype, byte[] data) {
        this.fileName = fileName;
        this.filetype = filetype;
        this.data = data;
    }

    public FileUploader() {

    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
