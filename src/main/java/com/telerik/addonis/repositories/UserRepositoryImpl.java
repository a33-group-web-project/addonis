package com.telerik.addonis.repositories;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Integer id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User findByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" from User where username= :usernameToCompare"
                    , User.class);
            query.setParameter("usernameToCompare", username);
            List<User> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User findUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" from User where email like :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

    @Override
    public User findUserByPhoneNumber(String phoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" from User where phoneNumber like :phoneNumber", User.class);
            query.setParameter("phoneNumber", phoneNumber);
            List<User> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "phone number", phoneNumber);
            }

            return result.get(0);
        }
    }

    @Override
    public User findUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public List<User> search(String keyword) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    " from User where  username like :username or email like :email or phoneNumber like :phoneNumber"
                    , User.class);

            query.setParameter("username", "%" + keyword + "%");
            query.setParameter("email", "%" + keyword + "%");
            query.setParameter("phoneNumber", "%" + keyword + "%");


            return query.list();
        }
    }

}
