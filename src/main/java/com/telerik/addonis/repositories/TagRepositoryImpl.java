package com.telerik.addonis.repositories;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Tags;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl extends TagRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tags> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tags> query = session.createQuery("from Tags", Tags.class);
            return query.list();
        }
    }

    @Override
    public Tags getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tags tag = session.get(Tags.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tags", id);
            }
            return tag;
        }
    }

    @Override
    public void create(Tags tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    @Override
    public void delete(int id) {
        Tags tagToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(tagToDelete);
            session.getTransaction().commit();
        }
    }

}
