package com.telerik.addonis.repositories;


import com.telerik.addonis.models.Details;

import java.util.List;

public interface DetailsRepository {

    Details getById(int id);

    List<Details> getAll();

    void create(Details details);
}
