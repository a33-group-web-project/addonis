package com.telerik.addonis.repositories;

import com.telerik.addonis.models.User;

import java.util.List;


public interface UserRepository {

    List<User> getAll();

    User getById(Integer id);

    void create(User user);

    void delete(Integer id);

    void update(User user);

    User findByUsername(String username);

    User findUserByEmail(String email);

    User findUserByPhoneNumber(String phoneNumber);

    User findUserById(int id);

    List<User> search(String keyword);

}
