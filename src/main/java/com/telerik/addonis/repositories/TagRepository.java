package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Tags;

import java.util.List;

public abstract class TagRepository {

    public abstract List<Tags> getAll();

    public abstract Tags getById(int id);

    public abstract void create(Tags tag);

    public abstract void delete(int id);
}
