package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Addon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileUploaderRepository extends JpaRepository<Addon, Integer> {


}
