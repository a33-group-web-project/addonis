package com.telerik.addonis.repositories;


import com.telerik.addonis.models.Rating;

import java.util.List;

public interface RatingRepository {

    Rating ratingExistsForAddonEntity(int id, int addonId);

    Rating getById(int id);

    void create(Rating rating);

    void update(Rating rating);

    double calculateRating(int id);

    List<Rating> getAll();

    Rating getRatingByUserId(int id);

    Rating getRatingById(int i);

}
