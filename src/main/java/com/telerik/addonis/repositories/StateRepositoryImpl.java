package com.telerik.addonis.repositories;

import com.telerik.addonis.models.State;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StateRepositoryImpl implements StateRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public StateRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<State> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<State> query = session.createQuery(
                    "from State ", State.class);
            return query.list();
        }
    }

    @Override
    public State getStateById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<State> query = session.createQuery(
                    "from State ", State.class);
            return query.list().get(0);
        }
    }
}
