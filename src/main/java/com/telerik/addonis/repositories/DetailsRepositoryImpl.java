package com.telerik.addonis.repositories;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Details;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DetailsRepositoryImpl implements DetailsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public DetailsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Details getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Details details = session.get(Details.class, id);
            if (details == null) {
                throw new EntityNotFoundException("Detail", id);
            }
            return details;
        }
    }

    @Override
    public List<Details> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Details> query = session.createQuery("from Details ", Details.class);
            return query.list();
        }
    }

    @Override
    public void create(Details details) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(details);
            session.getTransaction().commit();
        }
    }


}
