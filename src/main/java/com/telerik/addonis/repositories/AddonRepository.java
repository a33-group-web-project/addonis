package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Addon;


import java.util.List;

public interface AddonRepository {

    List<Addon> getAll(String name, String style, String sortBy, String sortOrder);

    List<Addon> search(String keyword);

    List<Addon> getAll();

    Addon getAddonById(int id);

    List<Addon> getByState(String state);

    Addon getByName(String name);

    void create(Addon addon);

    void update(Addon addon);

    void delete(int id);

    List<Addon> listNewAddons();

    List<Addon> listFeaturedAddons();

    List<Addon> listPopularAddons();
}
