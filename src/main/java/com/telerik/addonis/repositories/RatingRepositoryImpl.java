package com.telerik.addonis.repositories;


import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private final SessionFactory sessionFactory;
    private final UserRepository userRepository;
    private final AddonRepository addonRepository;

    public RatingRepositoryImpl(SessionFactory sessionFactory, UserRepository userRepository, AddonRepository addonRepository) {
        this.sessionFactory = sessionFactory;
        this.userRepository = userRepository;
        this.addonRepository = addonRepository;
    }

    @Override
    public Rating ratingExistsForAddonEntity(int id, int addonId) {
        try (Session session = sessionFactory.openSession()) {
            User user = userRepository.getById(id);
            Query<Rating> query = session.createQuery(" from Rating where user.id = :id and addon.id = :addonId ", Rating.class);
            query.setParameter("addonId", addonId);
            query.setParameter("id", id);
            if(query.list().size() == 0) {
                return null;
            }
            return query.list().get(0);
        }
    }

    @Override
    public Rating getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Rating rating = session.get(Rating.class, id);
            if (rating == null) {
                throw new EntityNotFoundException("Rating", id);
            }
            return rating;
        }
    }

    @Override
    public void create(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rating);
        }
    }

    @Override
    public void update(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public double calculateRating(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createQuery(String.format("SELECT avg(rating) from Rating where addon_id = :id",  Rating.class));
            query.setParameter("id", id);
            return query.getSingleResult();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public List<Rating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating ", Rating.class);
            return query.list();
        }

    }

    @Override
    public Rating getRatingByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating r where r.user.id = :userId", Rating.class);
            query.setParameter("userId", userId);

            List<Rating> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Rating", "userId", userId);
            }
            return result.get(0);
        }

    }

    @Override
    public Rating getRatingById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Rating rating = session.get(Rating.class, id);
            if (rating == null) {
                throw new EntityNotFoundException("Rating", id);
            }
            return rating;
        }

    }
}
