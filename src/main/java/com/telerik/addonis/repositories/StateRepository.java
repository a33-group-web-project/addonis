package com.telerik.addonis.repositories;

import com.telerik.addonis.models.State;

import java.util.List;

public interface StateRepository {

    List<State> getAll();

    State getStateById(int id);

}
