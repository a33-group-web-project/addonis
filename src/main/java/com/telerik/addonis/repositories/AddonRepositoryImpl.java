package com.telerik.addonis.repositories;


import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class AddonRepositoryImpl implements AddonRepository {

    private final List<Addon> addons;
    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRepositoryImpl(List<Addon> addons, SessionFactory sessionFactory) {
        this.addons = addons;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Addon> getAll(String name, String ide, String sortBy, String sortOrder) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon ", Addon.class);
            List<Addon> result;
            result = query.list();
            result = filterByName(result, name);
            result = filterByIde(result, ide);
            result = sortBy(result, sortBy);
            result = sortOrder(result, sortOrder);
            return result;
        }
    }

    @Override
    public List<Addon> search(String keyword) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(
                    " from Addon where  name like :name or ide like :ide"
                    , Addon.class);

            query.setParameter("name", "%" + keyword + "%");
            query.setParameter("ide", "%" + keyword + "%");


            return query.list();
        }
    }

    @Override
    public List<Addon> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon ", Addon.class);
            return query.list();
        }
    }

    private List<Addon> sortOrder(List<Addon> result, String order) {
        if (order == null || order.isBlank()) {
            return result;
        }
        if (order.equalsIgnoreCase("asc")) {
            Collections.reverse(result);
        }
        return result;
    }

    private List<Addon> sortBy(List<Addon> result, String sortBy) {
        try (Session session = sessionFactory.openSession()) {
            if (sortBy == null || sortBy.isBlank()) {
                return result;
            }
            switch (sortBy.toLowerCase()) {
                case "name":
                    result.sort(Comparator.comparing(Addon::getName));
                    break;
                case "numberofdownloads":
                    result.sort(Comparator.comparing(Addon::getNumberOfDownloads));
                    break;
                case "dateofcreation":
                    result.sort(Comparator.comparing(Addon::getDateOfCreation));
                    break;
                case "lastcommit":
                    result.sort(Comparator.comparing(Addon::getLastCommit));
                    break;

            }
            return result;
        }
    }

    private List<Addon> filterByName(List<Addon> result, String name) {
        if (name == null || name.isBlank()) {
            return result;
        }
        return Collections.singletonList(result.stream()
                .filter(addon -> addon.getName().contains(name))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("Addon", "name", name)));
    }

    private List<Addon> filterByIde(List<Addon> result, String ide) {
        if (ide == null || ide.isBlank()) {
            return result;
        }
        return Collections.singletonList(result.stream()
                .filter(ide1 -> ide1.getIde().equals(ide))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("Ide", "name", ide)));
    }

    @Override
    public Addon getAddonById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Addon addon = session.get(Addon.class, id);
            if (addon == null) {
                throw new EntityNotFoundException("Addon", id);
            }
            return addon;
        }
    }

    @Override
    public List<Addon> getByState(String state) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where state.state =:state", Addon.class);
            query.setParameter("state", state);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("State", "type", state.toString());
            }
            return query.list();
        }
    }


    @Override
    public Addon getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.name = :name", Addon.class);
            query.setParameter("name", name);

            List<Addon> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Addon", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public void create(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.save(addon);
        }
    }

    @Override
    public void update(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(addon);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Addon addonToDelete = getAddonById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(addonToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Addon> listNewAddons() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.category = 'New'", Addon.class);
            return query.list();
        }
    }

    @Override
    public List<Addon> listFeaturedAddons() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.category = 'Featured'", Addon.class);
            return query.list();
        }
    }

    @Override
    public List<Addon> listPopularAddons() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon a where a.category = 'Popular'", Addon.class);
            return query.list();
        }
    }
}
