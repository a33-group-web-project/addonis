package com.telerik.addonis.services;


import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.repositories.AddonRepository;
import com.telerik.addonis.repositories.FileUploaderRepository;
import com.telerik.addonis.services.mappers.ModelMapperAddon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class AddonServiceImpl implements AddonService {

    private final AddonRepository repository;
    private final FileUploaderRepository fileUploaderRepository;
    private final ModelMapperAddon modelMapperAddon;

    @Autowired
    public AddonServiceImpl(AddonRepository repository, FileUploaderRepository fileUploaderRepository, ModelMapperAddon modelMapperAddon) {
        this.repository = repository;
        this.fileUploaderRepository = fileUploaderRepository;
        this.modelMapperAddon = modelMapperAddon;
    }

    @Override
    public List<Addon> getAll(String name, String ide, String sortBy, String sortOrder) {
        return repository.getAll(name, ide, sortBy, sortOrder);
    }

    @Override
    public List<Addon> search(String keyword) {
        return repository.search(keyword);
    }

    @Override
    public List<Addon> getAll() {
        return repository.getAll();
    }

    @Override
    public Addon getAddonById(int id) {
        Addon addon = repository.getAddonById(id);
        if (addon == null) {
            throw new EntityNotFoundException("Addon", id);
        }
        return addon;
    }


    @Override
    public List<Addon> getAddonByState(String state) {
        return repository.getByState(state);
    }

    @Override
    public void update(Addon addon) {
        boolean duplicateExists = true;
        try {
            Addon existingAddon = repository.getByName(addon.getName());
            if (existingAddon.getId() == addon.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }

        repository.update(addon);
    }

    @Override
    public void deleteAddon(Integer id) {
        if (repository.getAddonById(id) == null) {
            throw new EntityNotFoundException("Addon", id);
        }
        repository.delete(id);
    }

    @Override
    public List<Addon> listNewAddons() {
        if (repository.listNewAddons().isEmpty()) {
            throw new EntityNotFoundException("Addon", "category", "New");
        }

        return repository.listNewAddons();
    }


    @Override
    public void updateImage(Addon addon, byte[] image) {
        addon.setImage(image);
        repository.update(addon);
    }

    @Override
    public void updateBinary(Addon addon, byte[] binaryContent) {

        addon.setBinaryContent(binaryContent);
        repository.update(addon);
    }

    @Override
    public List<Addon> listFeaturedAddons() {

        if (repository.listFeaturedAddons().isEmpty()) {
            throw new EntityNotFoundException("Addon", "category", "Featured");
        }

        return repository.listFeaturedAddons();
    }

    @Override
    public List<Addon> listPopularAddons() {

        if (repository.listPopularAddons().isEmpty()) {
            throw new EntityNotFoundException("Addon", "category", "Popular");
        }

        return repository.listPopularAddons();
    }

    @Override
    public Addon getAddonByName(String name) {
        if (repository.getByName(name) == null) {
            throw new EntityNotFoundException("Addon", name, name);
        }
        return repository.getByName(name);
    }

    @Override
    public Addon saveFile(MultipartFile file, Addon addon) {
        String fileUploaderName = file.getOriginalFilename();
        boolean duplicateExists = true;
        try {
            modelMapperAddon.fromDto(addon, file.getBytes());
            repository.getByName(addon.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        } catch (DataIntegrityViolationException | IOException e) {
            e.printStackTrace();
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }

        return fileUploaderRepository.save(addon);
    }

    @Override
    public void create(Addon addon) {
        boolean duplicateExists = true;
        try {
            repository.getByName(addon.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }

        repository.create(addon);

    }

    @Override
    public Optional<Addon> getFile(int fieldId) {
        return fileUploaderRepository.findById(fieldId);
    }

    @Override
    public List<Addon> getFiles() {
        return fileUploaderRepository.findAll();
    }

    @Override
    public Addon saveImage(MultipartFile images, Addon addon) {
        try {
            modelMapperAddon.fromDtoImage(addon, images.getBytes());
//            repository.getByName(addon.getName());
        } catch (DataIntegrityViolationException | IOException e) {
            e.printStackTrace();
        }
        repository.update(addon);
        return addon;
    }
}
