package com.telerik.addonis.services;

import com.telerik.addonis.models.Tags;

import java.util.List;

public interface TagService {

    List<Tags> getAll();

    Tags getTagById(Integer id);

    void create(Tags addon);

    void delete(int id);
}
