package com.telerik.addonis.services;

import com.telerik.addonis.models.State;
import com.telerik.addonis.repositories.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StateServiceImpl implements StateService {

    private final StateRepository repository;

    @Autowired
    public StateServiceImpl(StateRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<State> getAll() {
        return repository.getAll();
    }


    @Override
    public State getStateById(Integer id) {
        return repository.getStateById(id);
    }
}
