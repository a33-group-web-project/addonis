package com.telerik.addonis.services.mappers;

import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.CreateUserDto;
import com.telerik.addonis.models.dto.UpdateUserDetailsDto;
import com.telerik.addonis.models.dto.UpdateUserProfileDto;
import com.telerik.addonis.repositories.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class UserMapper {

    private final UserRepository userRepository;

    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createFromDto(CreateUserDto createUserDto) {
        User user = new User();
        createUserFromDto(createUserDto, user);
        return user;
    }

    public User updateFromDto(UpdateUserDetailsDto updateUserDetailsDto, Integer id) {
        User user = userRepository.getById(id);
        updateUserFromDto(updateUserDetailsDto, user);
        return user;
    }

    private void updateUserFromDto(UpdateUserDetailsDto updateUserDetailsDto, User user) {
        user.setFirstName(updateUserDetailsDto.getFirstName());
        user.setLastName(updateUserDetailsDto.getLastName());
        user.setPassword(updateUserDetailsDto.getPassword());
        user.setPhoneNumber(updateUserDetailsDto.getPhoneNumber());
        user.setEmail(updateUserDetailsDto.getEmail());
    }

    private void createUserFromDto(CreateUserDto createUserDto, User user) {
        user.setFirstName(createUserDto.getFirstName());
        user.setLastName(createUserDto.getLastName());
        user.setUsername(createUserDto.getUsername());
        user.setPassword(createUserDto.getPassword());
        user.setPhoneNumber(createUserDto.getPhoneNumber());
        user.setEmail(createUserDto.getEmail());
        user.setEnabled(true);
        user.setRoleId(1);

    }

    public User updateFromUpdateUserProfileDto(User user,
                                               UpdateUserProfileDto userProfileDto,
                                               MultipartFile photo) throws IOException {

        user.setFirstName(userProfileDto.getFirstName());
        user.setLastName(userProfileDto.getLastName());
        user.setEmail(userProfileDto.getEmail());
        user.setPhoneNumber(userProfileDto.getPhoneNumber());
        user.setPassword(userProfileDto.getPassword());
        user.setPhoto(photo.getBytes());

        return user;
    }
}
