package com.telerik.addonis.services.mappers;

import com.telerik.addonis.models.*;
import com.telerik.addonis.models.dto.AddonDto;
import com.telerik.addonis.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class ModelMapperAddon {
    private final AddonRepository addonRepository;
    private final StateRepository stateRepository;
    private final UserRepository userRepository;
    private final DetailsRepository detailsRepository;
    private final RatingRepository ratingRepository;
    private final TagRepository tagRepository;

    @Autowired
    public ModelMapperAddon(AddonRepository addonRepository, StateRepository stateRepository,
                            UserRepository userRepository, DetailsRepository detailsRepository, RatingRepository ratingRepository, TagRepository tagRepository) {
        this.addonRepository = addonRepository;
        this.stateRepository = stateRepository;
        this.userRepository = userRepository;
        this.detailsRepository = detailsRepository;
        this.ratingRepository = ratingRepository;
        this.tagRepository = tagRepository;
    }

    public Addon fromDto(Addon addon, byte[] addonFiles) {
        dtoToObjectMvc(addon, addonFiles);
        return addon;
    }

    private void dtoToObjectMvc(Addon addon, byte[] file) {
        State state = stateRepository.getStateById(1);
        addon.setBinaryContent(file);
        addon.setState(state);

    }

    public Addon fromDto(AddonDto addonDto, int addonId, User owner) {
        Addon addon = addonRepository.getAddonById(addonId);
        dtoToObject(addonDto, addon, owner);
        return addon;
    }


    public Addon fromDto(AddonDto addonDto, User user) {
        Addon addon = new Addon();
        dtoToObject(addonDto, addon, user);
        return addon;
    }

    private void dtoToObject(AddonDto addonDto, Addon addon, User owner) {
        State state = stateRepository.getStateById(1);
        Rating rating = ratingRepository.getRatingById(1);
        Tags tags = tagRepository.getById(addonDto.getTags());
        addon.setName(addonDto.getName());
        addon.setDescription(addonDto.getDescription());
        addon.setNumberOfDownloads(0);
        addon.setOriginLink(addonDto.getOriginLink());
        addon.setImage(addonDto.getImage());
        addon.setIde(addonDto.getIde());
        addon.setTags(tags);
        addon.setDateOfCreation(LocalDate.now());
        addon.setCreator(owner);
        addon.setState(state);
        addon.setRating(rating);
        if (addon.getCategory() == null) {
            addon.setCategory("New");
        }
        if (addon.getCategory().equals("New")) {
            addon.setCategory("New");
        }
    }

    public Addon fromDtoImage(Addon addon, byte[] image) {
        dtoToObjectMvcImage(addon, image);
        return addon;
    }

    private void dtoToObjectMvcImage(Addon addon, byte[] image) {
        State state = stateRepository.getStateById(1);
        addon.setImage(image);
        addon.setState(state);

    }

}
