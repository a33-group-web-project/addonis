package com.telerik.addonis.services;

import com.telerik.addonis.models.Details;


import java.util.List;

public interface DetailService {

    String getRepositoryName(String link);

    String getOwner(String link);

    Details consumeOpenIssuesCount(String owner, String repo, Details details);

    Details consumePullRequestCount(String owner, String repo, Details details);

    Details consumeCommits(String owner, String repo, Details details);

    List<Details> getAll();

    Details getDetailById(Integer id);

    void create(Details details);
}
