package com.telerik.addonis.services;

import com.telerik.addonis.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getUserByUsername(String username);

    User getUserById(Integer id);

    List<User> search(String keyword);

    void deleteUser(Integer id);

    void create(User user);

    void update(User userToBeUpdated, User updater);

    void update(User user);

    void updatePhoto(User userToBeUpdated, User updater, byte[] photoBytes);

    void setUserStatus(User user, String status);
}
