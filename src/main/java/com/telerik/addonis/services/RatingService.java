package com.telerik.addonis.services;

import com.telerik.addonis.models.Rating;


public interface RatingService {

    void create(Rating rating);

    void update(Rating rating);

    double calculateRating(int id);

    Rating getRatingByUserId(Integer userId);

}
