package com.telerik.addonis.services;


import com.telerik.addonis.models.Rating;
import com.telerik.addonis.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }


    @Override
    public void create(Rating rating) {
        Rating r = ratingRepository.ratingExistsForAddonEntity(rating.getUser().getId(), rating.getAddon().getId());
        if (r != null) {
            rating.setId(r.getId());
            ratingRepository.update(rating);
        } else {
            ratingRepository.create(rating);
        }
    }

    @Override
    public void update(Rating rating) {
        ratingRepository.update(rating);
    }

    @Override
    public double calculateRating(int id) {
        return ratingRepository.calculateRating(id);
    }

    @Override
    public Rating getRatingByUserId(Integer userId) {
        return ratingRepository.getRatingByUserId(userId);
    }

}
