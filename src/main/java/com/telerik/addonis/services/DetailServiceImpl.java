package com.telerik.addonis.services;

import com.telerik.addonis.models.Details;
import com.telerik.addonis.repositories.DetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;

@Service
public class DetailServiceImpl implements DetailService {

    private final DetailsRepository repository;

    @Autowired
    public DetailServiceImpl(DetailsRepository repository) {
        this.repository = repository;
    }

    @Override
    public String getOwner(String link) {
        String[] arr = link.split("/");
        return arr[3];
    }

    @Override
    public String getRepositoryName(String link) {
        String[] arr = link.split("/");
        return arr[4];
    }


    @Override
    public Details consumeOpenIssuesCount(String owner, String repo, Details details) {

        try {

            String url = String.format("https://api.github.com/repos/%s/%s/issues", owner, repo);
            String charset = StandardCharsets.UTF_8.name();
            String state = "open";
            String query = String.format("state=%s", URLEncoder.encode(state, charset));
            URL connection = new URL(url + "?" + query);
            connection.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.openStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String inputLine;
            String[] str;
            String[] count;

            while ((inputLine = in.readLine()) != null) {
                str = inputLine.split(",");
                if (str.length == 1) {
                    stringBuilder.append(0);
                    break;
                }

                count = str[8].split(":");
                stringBuilder.append(count[1]);
            }
            in.close();

            details.setOpenIssues(Integer.parseInt(stringBuilder.toString()));
            return details;

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

    }

    @Override
    public Details consumePullRequestCount(String owner, String repo, Details details) {
        try {

            String url = String.format("https://api.github.com/repos/%s/%s/pulls", owner, repo);
            String charset = StandardCharsets.UTF_8.name();
            URL connection = new URL(url);
            connection.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.openStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String inputLine;
            String[] str;
            String[] count;

            while ((inputLine = in.readLine()) != null) {
                str = inputLine.split(",");
                if (str.length == 1) {
                    stringBuilder.append(0);
                    break;
                }

                count = str[7].split(":");
                stringBuilder.append(count[1]);
            }
            String[] split = stringBuilder.toString().split(":");
            details.setPullRequest(Integer.parseInt(stringBuilder.toString()));
            in.close();
            return details;

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Details consumeCommits(String owner, String repo, Details details) {
        try {
            String url = String.format("https://api.github.com/repos/%s/%s/commits", owner, repo);
            String charset = StandardCharsets.UTF_8.name();
            URL connection = new URL(url);
            connection.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.openStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String inputLine;
            String[] str;
            String[] message;
            String[] date = new String[0];

            while ((inputLine = in.readLine()) != null) {
                str = inputLine.split(",");
                if (str.length == 1) {
                    stringBuilder.append(0);
                    break;
                }

                date = str[7].split(":");
                message = str[8].split(":");
                String reversedDate = date[1].toString().substring(1, 11);
                stringBuilder.append(reversedDate + ":");
                stringBuilder.append(message[1]);
            }
            in.close();
            String[] split = stringBuilder.toString().split(":");
            String refactoredDate = split[0];

            String RefactoredMessage = split[1];
            details.setLastCommit(LocalDate.parse(refactoredDate));
            details.setLastCommitMessage(RefactoredMessage);
            return details;

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }


    @Override
    public List<Details> getAll() {
        return repository.getAll();
    }

    @Override
    public Details getDetailById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public void create(Details details) {
        repository.create(details);
    }


}
