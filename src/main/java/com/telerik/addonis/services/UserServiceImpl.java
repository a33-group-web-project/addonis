package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.UnauthorizedOperationException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    public static final String INVALID_STATUS_MESSAGE = "Invalid status '%s'. Status must be 'true' or 'false'.";

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getUserByUsername(String username) {
        if (userRepository.findByUsername(username) == null) {
            throw new EntityNotFoundException("User", "username", username);
        }
        return userRepository.findByUsername(username);
    }

    @Override
    public User getUserById(Integer id) {
        if (userRepository.findUserById(id) == null) {
            throw new EntityNotFoundException("User", id);
        }
        return userRepository.findUserById(id);
    }

    @Override
    public List<User> search(String keyword) {
        return userRepository.search(keyword);
    }

    @Override
    public void deleteUser(Integer id) {
        if (userRepository.findUserById(id) == null) {
            throw new EntityNotFoundException("User", id);
        }
        userRepository.delete(id);
    }

    @Override
    public void create(User user) {
        boolean usernameExists = true;
        boolean emailExists = true;
        boolean phoneNumberExists = true;

        try {
            userRepository.findByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            usernameExists = false;
        }
        if (usernameExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        try {
            userRepository.findUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }
        if (emailExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        try {
            userRepository.findUserByPhoneNumber(user.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            phoneNumberExists = false;
        }
        if (phoneNumberExists) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }

        userRepository.create(user);
    }

    @Override
    public void update(User userToBeUpdated, User updater) {
        if (!updater.getUsername().equals(userToBeUpdated.getUsername())) {
            throw new UnauthorizedOperationException("You can update only your own profile!");
        }
        userWithDuplicateEmailExists(userToBeUpdated);
        userWithDuplicatePhoneExists(userToBeUpdated);

        userRepository.update(userToBeUpdated);
    }

    @Override
    public void update(User user) {
        userRepository.update(user);
    }

    @Override
    public void updatePhoto(User userToBeUpdated, User updater, byte[] photoBytes) {
        if (!updater.getUsername().equals(userToBeUpdated.getUsername())) {
            throw new UnauthorizedOperationException("You can update only your own photo!");
        }
        userToBeUpdated.setPhoto(photoBytes);
        userRepository.update(userToBeUpdated);
    }

    private void userWithDuplicatePhoneExists(User userToBeUpdated) {
        boolean phoneNumberExists = true;
        try {
            User existingUser = userRepository.findUserByPhoneNumber(userToBeUpdated.getPhoneNumber());
            if (!existingUser.getPhoneNumber().equals(userToBeUpdated.getPhoneNumber())) {
                phoneNumberExists = false;
            }
        } catch (EntityNotFoundException e) {
            phoneNumberExists = false;
        }
        if (phoneNumberExists) {
            throw new DuplicateEntityException("User", "phone number", userToBeUpdated.getPhoneNumber());
        }
    }

    private void userWithDuplicateEmailExists(User userToBeUpdated) {
        boolean emailExists = true;
        try {
            User existingUser = userRepository.findUserByEmail(userToBeUpdated.getEmail());
            if (!existingUser.getEmail().equals(userToBeUpdated.getEmail())) {
                emailExists = false;
            }
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }
        if (emailExists) {
            throw new DuplicateEntityException("User", "email", userToBeUpdated.getEmail());
        }
    }

    @Override
    public void setUserStatus(User user, String status) {
        boolean statusToSet;
        if (status.equals("true")) {
            statusToSet = true;
        } else if (status.equals("false")) {
            statusToSet = false;
        } else {
            throw new IllegalArgumentException(String.format(INVALID_STATUS_MESSAGE, status));
        }
        user.setEnabled(statusToSet);
        userRepository.update(user);
    }


}
