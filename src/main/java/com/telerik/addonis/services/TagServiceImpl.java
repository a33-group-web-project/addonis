package com.telerik.addonis.services;

import com.telerik.addonis.models.Tags;
import com.telerik.addonis.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Tags> getAll() {
        return repository.getAll();
    }


    @Override
    public Tags getTagById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public void create(Tags addon) {
        repository.create(addon);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }


}
