package com.telerik.addonis.services;

import com.telerik.addonis.models.Addon;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;


public interface AddonService {

    List<Addon> getAll(String name, String ide, String sortBy, String sortOrder);

    List<Addon> search(String keyword);

    List<Addon> getAll();

    Addon getAddonById(int id);

    List<Addon> getAddonByState(String name);

    List<Addon> listFeaturedAddons();

    List<Addon> listPopularAddons();

    Addon getAddonByName(String name);

    void create(Addon addon);

    void update(Addon addon);

    void deleteAddon(Integer id);

    List<Addon> listNewAddons();

    void updateBinary(Addon addon, byte[] bytes);

    void updateImage(Addon addon, byte[] bytes);

    Addon saveFile(MultipartFile file, Addon addon);

    Optional<Addon> getFile(int fieldId);

    List<Addon> getFiles();

    Addon saveImage(MultipartFile images, Addon addon);

}
