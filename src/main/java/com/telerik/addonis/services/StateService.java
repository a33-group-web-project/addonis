package com.telerik.addonis.services;

import com.telerik.addonis.models.State;


import java.util.List;

public interface StateService {

    List<State> getAll();

    State getStateById(Integer id);
}
