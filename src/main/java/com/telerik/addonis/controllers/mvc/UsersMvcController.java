package com.telerik.addonis.controllers.mvc;

import com.telerik.addonis.controllers.AuthenticationHelper;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.SearchUserDto;
import com.telerik.addonis.models.dto.UpdateUserProfileDto;
import com.telerik.addonis.services.UserService;
import com.telerik.addonis.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class UsersMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UsersMvcController(UserService userService,
                              UserMapper userMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/users")
    public String getAll(Model model) {
        model.addAttribute("allUsers", userService.getAll());
        model.addAttribute("searchUserDto", new SearchUserDto());
        return "users-list";
    }

    @PostMapping("/{id}/enable")
    public String enableUser(@PathVariable int id) {
        User user = userService.getUserById(id);
        userService.setUserStatus(user, "true");
        return "redirect:/users";
    }

    @PostMapping("/{id}/disable")
    public String disableUser(@PathVariable int id) {
        User user = userService.getUserById(id);
        userService.setUserStatus(user, "false");
        return "redirect:/users";
    }

    @PostMapping("/users/search")
    public String search(@ModelAttribute SearchUserDto searchUserDto, Model model) {
        List<User> searchResult = userService.search(searchUserDto.keyword);
        model.addAttribute("allUsers", searchResult);
        return "users-list";
    }

    @GetMapping("/user")
    public String showUserProfilePage(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("user", user);
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
        return "user-profile";
    }

    @GetMapping("user/update")
    public String showUpdateUserProfilePage(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("userProfile", user);

        } catch (EntityNotFoundException e) {
            return "not-found";
        }
        return "user-update";
    }

    @PostMapping("user/update")
    public String updateUserProfile(@Valid @ModelAttribute("userProfile") UpdateUserProfileDto userProfileDto,
                                    BindingResult error,
                                    Model model,
                                    HttpSession session,
                                    @RequestParam("photo") MultipartFile photo) {

        if (error.hasErrors()) {
            model.addAttribute("error", "The required fields cannot be empty.");
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            userMapper.updateFromUpdateUserProfileDto(user, userProfileDto, photo);
            userService.update(user);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/user";
    }

}
