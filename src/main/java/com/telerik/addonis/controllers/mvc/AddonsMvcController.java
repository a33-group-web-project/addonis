package com.telerik.addonis.controllers.mvc;

import com.telerik.addonis.exceptions.AuthenticationFailureException;
import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.UnauthorizedOperationException;
import com.telerik.addonis.models.*;
import com.telerik.addonis.models.dto.AddonDto;
import com.telerik.addonis.models.dto.SearchAddonDto;
import com.telerik.addonis.services.AddonService;
import com.telerik.addonis.services.DetailService;
import com.telerik.addonis.services.RatingService;
import com.telerik.addonis.services.TagService;
import com.telerik.addonis.services.mappers.ModelMapperAddon;
import com.telerik.addonis.services.mappers.RatingModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
@RequestMapping("/addons")
public class AddonsMvcController {

    public static final String MODIFY_ADDON_ERROR_MESSAGE = "Only owner or admin can modify a add-on.";
    private final AddonService addonService;
    private final ModelMapperAddon modelMapperAddon;
    private final DetailService detailService;
    private final AuthenticationController authenticationHelper;
    private final RatingService ratingService;
    private final RatingModelMapper ratingModelMapper;
    private final TagService tagService;

    @Autowired
    public AddonsMvcController(AddonService addonService, ModelMapperAddon modelMapperAddon,
                               DetailService detailService, AuthenticationController authenticationHelper, RatingService ratingService, RatingModelMapper ratingModelMapper, TagService tagService) {
        this.addonService = addonService;
        this.modelMapperAddon = modelMapperAddon;
        this.detailService = detailService;
        this.authenticationHelper = authenticationHelper;
        this.ratingService = ratingService;
        this.ratingModelMapper = ratingModelMapper;
        this.tagService = tagService;
    }

    @GetMapping
    public String showAllAddons(Model model, HttpSession session) {

        LinkedHashMap<String, List<Addon>> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("Featured", addonService.listFeaturedAddons());
        linkedHashMap.put("New", addonService.listNewAddons());
        linkedHashMap.put("Popular", addonService.listPopularAddons());
        model.addAttribute("addonsData", linkedHashMap);
        model.addAttribute("searchAddonDto", new SearchAddonDto());
        return "addons";
    }

    @ModelAttribute("details")
    public List<Details> populateDetails() {
        return detailService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tags> populateTags() {
        return tagService.getAll();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/new")
    public String showNewAddonPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("addon", new AddonDto());
        return "addon-new";
    }

    @GetMapping("/")
    public String get(Model model) {
        List<Addon> addonFiles = addonService.getFiles();
        model.addAttribute("addonFiles", addonFiles);
        return "addon-new";
    }

    @PostMapping("/uploadFiles")
    public String uploadAddonFile(@RequestParam("files") MultipartFile files,
                                  @ModelAttribute("addonDto") AddonDto addonDto,
                                  BindingResult errors,
                                  Model model,
                                  HttpSession session) {
        User user;
        Addon addon = new Addon();
        Details details = new Details();
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "addon-new";
        }

        try {
            String owner = detailService.getOwner(addonDto.getOriginLink());
            addon = modelMapperAddon.fromDto(addonDto, user);
            String repoName = detailService.getRepositoryName(addon.getOriginLink());
            detailService.consumeOpenIssuesCount(owner, repoName, details);
            detailService.consumePullRequestCount(owner, repoName, details);
            detailService.consumeCommits(owner, repoName, details);
            detailService.create(details);
            addon.setDetails(details);
            addonService.saveFile(files, addon);
            return "redirect:/addons";
        } catch (DuplicateEntityException e) {
            throw new DuplicateEntityException("Add-on", "name", addon.getName());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }

    @GetMapping("/{id}")
    public String showSingleAddon(@PathVariable int id, Model model, HttpSession session) {
        try {
//            User user = authenticationHelper.tryGetUser(session);
            Addon addon = addonService.getAddonById(id);
            double rating = ratingService.calculateRating(addon.getId());
            model.addAttribute("rating", rating);
            model.addAttribute("addon", addon);
            return "addon";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }


    @GetMapping("/{id}/update")
    public String showEditAddonPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Addon addon = addonService.getAddonById(id);
            if (!addon.getCreator().getUsername().equals(user.getUsername())) {
                throw new UnauthorizedOperationException(MODIFY_ADDON_ERROR_MESSAGE);
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Addon addon = addonService.getAddonById(id);
            model.addAttribute("addon", addon);
            return "addon-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("{addonId}/update")
    public String updateAddon(@PathVariable("addonId") int id,
                              @Valid @ModelAttribute("addon") AddonDto dto,
                              @RequestParam("images") MultipartFile images,
                              BindingResult errors,
                              Model model,
                              HttpSession session) {

        Addon addon = new Addon();
        Details details = new Details();
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "addon-update";
        }

        try {
            String owner = detailService.getOwner(dto.getOriginLink());
            addon = modelMapperAddon.fromDto(dto, id, user);
            String repoName = detailService.getRepositoryName(dto.getOriginLink());
            detailService.consumeOpenIssuesCount(owner, repoName, details);
            detailService.consumePullRequestCount(owner, repoName, details);
            detailService.consumeCommits(owner, repoName, details);
            detailService.create(details);
            addon.setDetails(details);
            addonService.saveImage(images, addon);
            return "redirect:/addons";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_addon", e.getMessage());
            return "addon-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteAddon(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            Addon addon = addonService.getAddonById(id);
            if (!addon.getCreator().getUsername().equals(user.getUsername())) {
                throw new UnauthorizedOperationException(MODIFY_ADDON_ERROR_MESSAGE);
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        try {
            addonService.deleteAddon(id);
            return "redirect:/addons";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/addon/search")
    public String filter(@ModelAttribute SearchAddonDto searchAddonDtoDto, Model model) {
        LinkedHashMap<String, List<Addon>> addonList = new LinkedHashMap<>();
        List<Addon> search = addonService.search(searchAddonDtoDto.keyword);
        addonList.put("Searched Аdd-ons", search);
        for (List<Addon> value : addonList.values()) {
            if (value.isEmpty()) {
                return "not-found";
            }
        }
        model.addAttribute("addonsList", addonList);
        return "addons-search";
    }
}
