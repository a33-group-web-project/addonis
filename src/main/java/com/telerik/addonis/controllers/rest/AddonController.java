package com.telerik.addonis.controllers.rest;


import com.telerik.addonis.controllers.AuthenticationHelper;
import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.UnauthorizedOperationException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Details;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.AddonDto;
import com.telerik.addonis.models.dto.RatingDto;
import com.telerik.addonis.services.AddonService;
import com.telerik.addonis.services.DetailService;
import com.telerik.addonis.services.RatingService;
import com.telerik.addonis.services.mappers.ModelMapperAddon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/addons")
public class AddonController {

    private final AddonService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapperAddon modelMapperAddon;
    private final DetailService detailService;
    private final RatingService ratingService;


    @Autowired
    public AddonController(AddonService service, AuthenticationHelper authenticationHelper
            , ModelMapperAddon modelMapperAddon, DetailService detailService, RatingService ratingService) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapperAddon = modelMapperAddon;
        this.detailService = detailService;
        this.ratingService = ratingService;
    }

    @GetMapping()
    public List<Addon> getAllAddons(@RequestParam(required = false) String name,
                                    @RequestParam(required = false) String ide,
                                    @RequestParam(required = false) String sortBy,
                                    @RequestParam(required = false) String sortOrder) {

        return service.getAll(name, ide, sortBy, sortOrder);
    }

    @GetMapping("/{id}")
    public Addon getAddonById(@PathVariable Integer id) {
        try {
            return service.getAddonById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/addonstate/{state}")
    public List<Addon> getAddonByState(@PathVariable String state) {
        try {
            return service.getAddonByState(state);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/addonname/{username}")
    public Addon getAddonByName(@PathVariable String username) {
        try {
            return service.getAddonByName(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Addon create(@RequestHeader HttpHeaders headers, @Valid @RequestBody AddonDto addonDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Addon addon = new Addon();
            Details details = new Details();
            String owner = detailService.getOwner(addonDto.getOriginLink());
            addon = modelMapperAddon.fromDto(addonDto, user);
            String repoName = detailService.getRepositoryName(addon.getOriginLink());
            detailService.consumeOpenIssuesCount(owner, repoName, details);
            detailService.consumePullRequestCount(owner, repoName, details);
            detailService.consumeCommits(owner, repoName, details);
            detailService.create(details);
            addon.setDetails(details);
            service.create(addon);
            return addon;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/rate/{addonId}")
    public Addon rateAddon(@PathVariable Integer addonId, @Valid @RequestBody RatingDto ratingDto,
                           @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Rating rate = ratingService.getRatingByUserId(user.getId());
            rate.setRating(ratingDto.getRating());
            Addon addon = getAddonById(addonId);
            addon.setRating(rate);
            return addon;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());

        }
    }


    @PutMapping("{id}/binaryContent")
    public Addon updateAddonBinary(@PathVariable int id,
                                   @RequestParam("binaryContent") MultipartFile file,
                                   @RequestHeader HttpHeaders headers) throws IOException {
        try {
            User updater = authenticationHelper.tryGetUser(headers);
            Addon addon = service.getAddonById(id);
            service.updateBinary(addon, file.getBytes());
            return addon;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());

        }
    }

    @PutMapping("{id}/image")
    public Addon updateAddonImage(@PathVariable Integer id,
                                  @RequestPart("image") MultipartFile photoFile,
                                  @RequestHeader HttpHeaders headers) throws IOException {
        try {
            User updater = authenticationHelper.tryGetUser(headers);
            Addon addon = service.getAddonById(id);
            service.updateImage(addon, photoFile.getBytes());
            return addon;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());

        }
    }

    @PutMapping("/{id}")
    public Addon update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody AddonDto addonDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Addon addon = new Addon();
            String owner = detailService.getOwner(addon.getOriginLink());
            addon = modelMapperAddon.fromDto(addonDto, id, user);
            service.update(addon);
            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        try {
            service.deleteAddon(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/listNew/")
    public List<Addon> listNewAddons() {
        try {
            return service.listNewAddons();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/listFeatured/")
    public List<Addon> listFeaturedAddons() {
        try {
            return service.listFeaturedAddons();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/listPopular/")
    public List<Addon> listPopularAddons() {
        try {
            return service.listPopularAddons();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
