package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.State;
import com.telerik.addonis.services.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/states")
public class StateController {

    private final StateService service;

    @Autowired
    public StateController(StateService service) {
        this.service = service;
    }

    @GetMapping
    public List<State> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public State getById(@PathVariable int id) {
        try {
            return service.getStateById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}