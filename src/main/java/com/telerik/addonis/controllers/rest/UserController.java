package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.controllers.AuthenticationHelper;
import com.telerik.addonis.exceptions.DuplicateEntityException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.UnauthorizedOperationException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.CreateUserDto;
import com.telerik.addonis.models.dto.UpdateUserDetailsDto;
import com.telerik.addonis.services.UserService;
import com.telerik.addonis.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          UserMapper userMapper,
                          AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAllUsers(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetAdmin(headers);
        return userService.getAll();
    }

    @GetMapping("/id/{id}")
    public User getUserById(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        User user = authenticationHelper.tryGetAdmin(headers);
        try {
            return userService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/username/{username}")
    public User getUserByUsername(@RequestHeader HttpHeaders headers, @PathVariable String username) {
        User user = authenticationHelper.tryGetAdmin(headers);
        try {
            return userService.getUserByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers, @RequestParam(name = "keyword") String keyword) {
        User user = authenticationHelper.tryGetAdmin(headers);
        try {
            return userService.search(keyword);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@Valid @RequestBody CreateUserDto createUserDto) {

        try {
            User user = userMapper.createFromDto(createUserDto);
            userService.create(user);

            return user;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("{id}/details")
    public User updateUserDetails(@PathVariable Integer id,
                                  @RequestBody UpdateUserDetailsDto updateUserDetailsDto,
                                  @RequestHeader HttpHeaders headers) {
        try {
            User updater = authenticationHelper.tryGetUser(headers);
            User userToBeUpdated = userMapper.updateFromDto(updateUserDetailsDto, id);
            userService.update(userToBeUpdated, updater);
            return userToBeUpdated;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());
        }
    }

    @PutMapping("{id}/photo")
    public User updateUserPhoto(@PathVariable Integer id,
                                @RequestPart("photo") MultipartFile photoFile,
                                @RequestHeader HttpHeaders headers) throws IOException {
        try {
            User updater = authenticationHelper.tryGetUser(headers);
            User userToBeUpdated = userService.getUserById(id);
            userService.updatePhoto(userToBeUpdated, updater, photoFile.getBytes());
            return userToBeUpdated;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException((HttpStatus.UNAUTHORIZED), e.getMessage());

        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        User user = authenticationHelper.tryGetAdmin(headers);
        try {
            userService.deleteUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PostMapping("{id}/status")
    public User setUserStatus(@RequestHeader HttpHeaders headers, @PathVariable Integer id, @RequestParam String enabled) {
        User user = authenticationHelper.tryGetAdmin(headers);
        User userToUpdate = userService.getUserById(id);
        try {
            userService.setUserStatus(userToUpdate, enabled);
            return userToUpdate;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
