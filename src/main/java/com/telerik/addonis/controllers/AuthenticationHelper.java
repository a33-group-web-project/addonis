package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.AuthenticationFailureException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";

    private final UserService userService;


    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }


    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested resource requires authentication.");
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getUserByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
        }
    }

    public User tryGetAdmin(HttpHeaders headers) {
        if (!tryGetUser(headers).hasAdminRole()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return tryGetUser(headers);
    }


    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException("You are not logged.");
        }

        return userService.getUserByUsername(currentUser);
    }

    public User tryGetAdmin(HttpSession session) {
        if (!tryGetUser(session).hasAdminRole()) {
            throw new AuthenticationFailureException("You are not authorized to view this page");
        }
        return tryGetUser(session);
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getUserByUsername(username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }
}

