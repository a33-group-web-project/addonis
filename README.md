# A33 Group Web Project Addonis

Addonis is an Addons Registry web application. Some of the functionality Addonis provides to users include:

- Create a user profile
- Update/Edit user profile
- Publish an addon
- Edit/Update addon

Addonis database model:

![](C:\Users\placebo\Desktop\Repos\addonis\src\main\resources\static\addonis_db_schema.png)

To run the application:

1. Clone the project.
2. Run the dependencies and build.gradle.
3. Run the database scripts to create the tables and import the data.
4. Start AddonisApplication.

The Swagger API Documentation is available at: http://localhost:8080/swagger-ui.html

Created by Yordan Georgiev and Kaloyan Haralampiev.
